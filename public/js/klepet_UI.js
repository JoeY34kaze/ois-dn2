
var besede=[];
var socket = io.connect();
var trenutniVzdevek="Gost1";
var trenutniKanal;
var f=false;

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo=cenzura(sporocilo);
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function getPike(stev){
  var rez="";
  for(var i=0;0<stev;i++){
    rez+='*';
  }
  return rez;
}
function cenzura(sporocilo){
  var spor=sporocilo.split(" ");
  
  for(var i=0;i<besede.length;i++){
    for(var j=0;j<spor.length;j++){
      if(spor[j] === besede[i]){sporocilo=sporocilo.replace(besede[i],getPike(besede[i].length))}
    }
  }
  return sporocilo;
}

var socket = io.connect();

$(document).ready(function() {
  $.get('swearWords.txt', function(data){
   besede = data.split('\n');
  });
  var klepetApp = new Klepet(socket);
  var zacetniVzdevek="Gost1";
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      zacetniVzdevek=rezultat.vzdevek;
      trenutniVzdevek=zacetniVzdevek;
      if(f){$('#kanal').text(trenutniVzdevek+" @ "+trenutniKanal);}
      f=true;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(trenutniVzdevek+" @ "+rezultat.kanal);
    trenutniKanal=rezultat.kanal;
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal !== '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});